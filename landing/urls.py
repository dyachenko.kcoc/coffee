from django.urls import path
# from . import views           # для articles
from landing.views import coffee_grinding, home_coffee, how_prepare_coffee #, CreatePostView, test_home, just_text,

urlpatterns = [
    path('', home_coffee, name='home'),
  #  path('home.html', home_coffee, name='home_coffee'),
    path('prepare/', how_prepare_coffee, name='prepare'),
    path('grinding/', coffee_grinding, name='grinding'),
    # path('conversation.html', CreatePostView.as_view(), name='conversation_coffee'),
    # path('test_text', just_text, name='text'),
    # path('test_home', test_home, name='test_home'),
    # path('articles/<int:year>/', views.year_archive),
    #path('articles/<int:year>/<int:month>/', views.month_archive),
    #path('articles/<int:year>/<int:month>/<int:pk>/', views.article_detail),
        # для URL:“/ articles / 2005/05/39323 /”
        # Django викличе функцію .news.views.article_detail(request, year=2005, month=5, pk=39323)
    # path('polls.html', views.polls),


]
