from django.contrib import admin
from landing.models import Landing, Question, Choice, CssCoffee, HomeCoffee, Article, Visitor #,PostMaker


@admin.register(Landing)
class LandingAdmin(admin.ModelAdmin):
    pass


@admin.register(CssCoffee)
class CssCoffeeAdmin(admin.ModelAdmin):
    pass


@admin.register(HomeCoffee)
class HomeCoffee(admin.ModelAdmin):
    pass


# @admin.register(PostMaker)
# class PostMaker(admin.ModelAdmin):
#     pass


@admin.register(Article)
class Article(admin.ModelAdmin):
    pass


@admin.register(Visitor)
class Visitor(admin.ModelAdmin):
    pass

@admin.register(Question)
class Question(admin.ModelAdmin):
    pass


@admin.register(Choice)
class Choice(admin.ModelAdmin):
    pass
