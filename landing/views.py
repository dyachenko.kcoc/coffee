

from django.shortcuts import render
from landing.models import Landing, CssCoffee, HomeCoffee, Article, Visitor, Question, Choice #PostMaker
# from django.http import HttpResponse
# from django.views.generic import ListView, CreateView
# from django.urls import reverse_lazy
# from .forms import PostForm


# коли запит(request) прийде в url, там має бути посилання на відповідну функцію в views,
# що обробить цей запит і дасть відповідь(response)


#  -----coffee project-------------------------------------
def home_coffee(request: object):
    template_name = 'landing/home_coffee.html'
    context = {
        'cap_of_coffee': Landing.objects.all()[1],
        'bg_content': CssCoffee.objects.all()[0],
        'bg_footer': CssCoffee.objects.all()[1],
        'bg_header': CssCoffee.objects.all()[2],
        'bg_headerleft': CssCoffee.objects.all()[3],
        'bg_leftnav_blue': CssCoffee.objects.all()[4],
        'bg_leftnav_brown': CssCoffee.objects.all()[5],
        'bg_orange': CssCoffee.objects.all()[6],
        'ctw_logo': CssCoffee.objects.all()[7],
        'header_coffeetea': CssCoffee.objects.all()[8],
        'logo': CssCoffee.objects.all()[9],
        'cap_vector': CssCoffee.objects.all()[10],
        'advertising': CssCoffee.objects.all()[11],
        'fight_club': HomeCoffee.objects.all()[0],
        'coffee_in_bed': HomeCoffee.objects.all()[1],
        'wolf': HomeCoffee.objects.all()[2],
        'for_him': HomeCoffee.objects.all()[3],
        'star3': Landing.objects.all()[22],
        'star': Landing.objects.all()[20],
        'art1': HomeCoffee.objects.all()[4],
        'art2': HomeCoffee.objects.all()[5],
        'art3': HomeCoffee.objects.all()[6],
    }
    return render(request, template_name=template_name, context=context)


def how_prepare_coffee(request, *args, **kwargs):
    template_name = 'landing/prepare_coffee.html'
    context = {
        'all_photo': Landing.objects.all(),
        'coffee_mashine': Landing.objects.all()[2],
        'east_coffee': Landing.objects.all()[3],
        'french': Landing.objects.all()[4],
        'geizer': Landing.objects.all()[5],
        'luvak': Landing.objects.all()[6],
        'esspresso': Landing.objects.all()[7],
        'childy': Landing.objects.all()[14],
        'glasse': Landing.objects.all()[8],
        'cappuccino': Landing.objects.all()[9],
        'latte_maciato': Landing.objects.all()[10],
        'moka': Landing.objects.all()[11],
        'ristretto': Landing.objects.all()[12],
        'phrapuchino': Landing.objects.all()[13],
        'bg_content': CssCoffee.objects.all()[0],
        'bg_footer': CssCoffee.objects.all()[1],
        'bg_header': CssCoffee.objects.all()[2],
        'bg_headerleft': CssCoffee.objects.all()[3],
        'bg_leftnav_blue': CssCoffee.objects.all()[4],
        'bg_leftnav_brown': CssCoffee.objects.all()[5],
        'bg_orange': CssCoffee.objects.all()[6],
        'ctw_logo': CssCoffee.objects.all()[7],
        'header_coffeetea': CssCoffee.objects.all()[8],
        'logo': CssCoffee.objects.all()[9],
        'cap_vector': CssCoffee.objects.all()[10],
        'advertising': CssCoffee.objects.all()[11],
        'star': Landing.objects.all()[20],
        'star2': Landing.objects.all()[21],
        'star3': Landing.objects.all()[22],

    }
    return render(request, template_name=template_name, context=context)


def coffee_grinding(request: object):
    template_name = 'landing/grinding_coffee.html'
    context = {
        'coffee_grind': Landing.objects.all()[15],
        'coffee_grind_blade': Landing.objects.all()[16],
        'coffee_grind_burr': Landing.objects.all()[17],
        'coffee_grind_size_large': Landing.objects.all()[18],
        'bg_content': CssCoffee.objects.all()[0],
        'bg_footer': CssCoffee.objects.all()[1],
        'bg_header': CssCoffee.objects.all()[2],
        'bg_headerleft': CssCoffee.objects.all()[3],
        'bg_leftnav_blue': CssCoffee.objects.all()[4],
        'bg_leftnav_brown': CssCoffee.objects.all()[5],
        'bg_orange': CssCoffee.objects.all()[6],
        'ctw_logo': CssCoffee.objects.all()[7],
        'header_coffeetea': CssCoffee.objects.all()[8],
        'logo': CssCoffee.objects.all()[9],
        'cap_vector': CssCoffee.objects.all()[10],
        'advertising': CssCoffee.objects.all()[11],
        'coffee_grind_size_chart': Landing.objects.all()[19],

    }
    return render(request, template_name=template_name, context=context)


# class CreatePostView(CreateView):
#     model = PostMaker
#     form_class = PostForm
#     success_url = reverse_lazy('home_coffee.html')
#     template_name = 'landing/conversation_coffee.html'

    # def get(self, request, *args, **kwargs):
    #     global template_name
    #     context = {
    #         'bg_content': CssCoffee.objects.all()[0],
    #         'bg_footer': CssCoffee.objects.all()[1],
    #         'bg_header': CssCoffee.objects.all()[2],
    #         'bg_headerleft': CssCoffee.objects.all()[3],
    #         'bg_leftnav_blue': CssCoffee.objects.all()[4],
    #         'bg_leftnav_brown': CssCoffee.objects.all()[5],
    #         'bg_orange': CssCoffee.objects.all()[6],
    #         'ctw_logo': CssCoffee.objects.all()[7],
    #         'header_coffeetea': CssCoffee.objects.all()[8],
    #         'logo': CssCoffee.objects.all()[9],
    #         'cap_vector': CssCoffee.objects.all()[10],
    #         'advertising': CssCoffee.objects.all()[11],
    #     }
    #     return render(request, template_name, context)


# -------training projects--------------------


# def test_home(request: object):
#     template_name = 'landing/home_coffee_page.html'
#     context = {
#         'first_card': Landing.objects.first(),
#         'all_photo': Landing.objects.all(),
#
#     }
#     return render(request, template_name=template_name, context=context)
#
#
# def just_text(request):
#     return HttpResponse("Text 'Hello' for exemple")
#

def year_archive(request, year):
    a_list = Article.objects.all()
    context = {'year': year,
               'article_list': a_list,
               'coffee_grind': Landing.objects.all()[15],
               'coffee_grind_blade': Landing.objects.all()[16],
               'coffee_grind_burr': Landing.objects.all()[17],
               'coffee_grind_size_large': Landing.objects.all()[18],
               'bg_content': CssCoffee.objects.all()[0],
               'bg_footer': CssCoffee.objects.all()[1],
               'bg_header': CssCoffee.objects.all()[2],
               'bg_headerleft': CssCoffee.objects.all()[3],
               'bg_leftnav_blue': CssCoffee.objects.all()[4],
               'bg_leftnav_brown': CssCoffee.objects.all()[5],
               'bg_orange': CssCoffee.objects.all()[6],
               'ctw_logo': CssCoffee.objects.all()[7],
               'header_coffeetea': CssCoffee.objects.all()[8],
               'logo': CssCoffee.objects.all()[9],
               'cap_vector': CssCoffee.objects.all()[10],
               'advertising': CssCoffee.objects.all()[11],
               'coffee_grind_size_chart': Landing.objects.all()[19],
               }
    return render(request, 'landing/year_archive.html', context=context)


def polls(request: object):
    template_name = 'landing/polls.html'
    context = {
        'coffee_grind': Landing.objects.all()[15],
        'coffee_grind_blade': Landing.objects.all()[16],
        'coffee_grind_burr': Landing.objects.all()[17],
        'coffee_grind_size_large': Landing.objects.all()[18],
        'bg_content': CssCoffee.objects.all()[0],
        'bg_footer': CssCoffee.objects.all()[1],
        'bg_header': CssCoffee.objects.all()[2],
        'bg_headerleft': CssCoffee.objects.all()[3],
        'bg_leftnav_blue': CssCoffee.objects.all()[4],
        'bg_leftnav_brown': CssCoffee.objects.all()[5],
        'bg_orange': CssCoffee.objects.all()[6],
        'ctw_logo': CssCoffee.objects.all()[7],
        'header_coffeetea': CssCoffee.objects.all()[8],
        'logo': CssCoffee.objects.all()[9],
        'cap_vector': CssCoffee.objects.all()[10],
        'advertising': CssCoffee.objects.all()[11],
        'coffee_grind_size_chart': Landing.objects.all()[19],

    }
