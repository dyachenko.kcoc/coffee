import datetime
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils import timezone


class Landing(models.Model):
    text = models.TextField(_('Назва фотографії'), null=True, blank=True)
    photo = models.ImageField(_('Фотографія'), upload_to='landing/%Y/%m/%d/', db_index=True)

    # landing located in directory which indicates in settings.py MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
    # in faces will created directory /%Y/%m/%d/ - year/month/day of time download photo
    def __str__(self):
        return f"{'id ='},{self.id},{' name foto = '},{self.text}"

    # Ми також включимо метод __str__ нижче, щоб заголовок з'явився в нашому адміністраторі Django пізніше.

    class Meta:
        verbose_name = _('Фотографія з назвою')
        verbose_name_plural = _('Фотографії з назвою')


class CssCoffee(models.Model):
    text = models.TextField(_('Назва LOGO'), null=True, blank=True)
    photo = models.ImageField(_('Картинка'), upload_to='CssCoffee/%Y/%m/%d/', db_index=True)

    def __str__(self):
        return f"{'id ='},{self.id},{' name foto = '},{self.text}"

    class Meta:
        verbose_name = _('Для CSS')
        verbose_name_plural = _('Для CSS')


class HomeCoffee(models.Model):
    text = models.TextField(_('Назва'), null=True, blank=True)
    photo = models.ImageField(_('Картинка'), upload_to='HomeCoffee/%Y/%m/%d/', db_index=True)

    def __str__(self):
        return f"{'id ='},{self.id},{' name foto = '},{self.text}"

    class Meta:
        verbose_name = _('HOME Foto')
        verbose_name_plural = _('HOME Fotos')

# -------- "давайте поспілкуємось" --------

# class PostMaker(models.Model):
#     text = models.TextField(_('Текст'), null=True, blank=True)
#     photo = models.ImageField(_('Фотографія'), upload_to='PostMaker/%Y/%m/%d/', db_index=True)
#
#     def __str__(self):
#         return f"{'id ='},{self.id},{' name foto = '},{self.text}"
#
#     class Meta:
#         verbose_name = _('New Post')
#         verbose_name_plural = _('All Posts')


# -------- django courses --------
class Visitor(models.Model):
    full_name = models.CharField(max_length=70)
    e_mail = models.CharField(max_length=25)

    def __str__(self):
        return self.full_name


class Article(models.Model):
    pub_date = models.DateField()
    headline = models.CharField(max_length=200)
    content = models.TextField()
    visitor = models.ForeignKey(Visitor, on_delete=models.CASCADE)

    def __str__(self):
        return self.headline


# --------Django courses basic--------

class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')

    def __str__(self):
        return self.question_text

    def was_published_recently(self):
        return self.pub_date >= (timezone.now() - datetime.timedelta(days=1)).timestamp()


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text
